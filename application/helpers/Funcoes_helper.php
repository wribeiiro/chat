<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if (!function_exists('formata_preco')) {

    function verificaModulo($modulo){

        if($modulo == strtoupper("pd")) {
			return "Principal/index";
		} else if($modulo == strtoupper("cr")) {
			return "CR/index";
		} else if($modulo == strtoupper("cp")) {
			return "CP/index";
		} else {
			die("Erro modulo ou nivel nao encontrado! " . $nomeModulo);
		}
    }
}
/**
 * Funcao que retorna o a sigla do estado para usar no HighMaps 
 * pega cod estado parametro
 * @param [string] $codEstado
 * @return 'sigla estado'
 */
function retornaEstadoMap($codEstado) {
	switch (strtoupper($codEstado)) {
		case 'SP':
			return 'br-sp';
			break;
		case 'MA':
			return 'br-ma';
			break;
		case 'PA':
			return 'br-pa';
			break;
		case 'SC':
			return 'br-sc';
			break;
		case 'BA':
			return 'br-ba';
			break;
		case 'AP':
			return 'br-ap';
			break;
		case 'MS':
			return 'br-ms';
			break;
		case 'MG':
			return 'br-mg';
			break;
		case 'GO':
			return 'br-go';
			break;
		case 'RS':
			return 'br-rs';
			break;
		case 'TO':
			return 'br-to';
			break;
		case 'PI':
			return 'br-pi';
			break;
		case 'AL':
			return 'br-al';
			break;
		case 'PB':
			return 'br-pb';
			break;
		case 'CE':
			return 'br-ce';
			break;
		case 'SE':
			return 'br-se';
			break;
		case 'RR':
			return 'br-rr';
			break;
		case 'PE':
			return 'br-pe';
			break;
		case 'PR':
			return 'br-pr';
			break;
		case 'ES':
			return 'br-es';
			break;
		case 'RJ':
			return 'br-rj';
			break;
		case 'RN':
			return 'br-rn';
			break;
		case 'AM':
			return 'br-am';
			break;
		case 'MT':
			return 'br-mt';
			break;
		case 'DF':
			return 'br-df';
			break;
		case 'AC':
			return 'br-ac';
			break;
		case 'RO':
			return 'br-ro';
			break;
		default:
			return 'br-sc';
	}
}