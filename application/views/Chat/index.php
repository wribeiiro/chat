<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Wellisson">
<title> Chat </title> 
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="<?=base_url('')?>assets/css/main.css" type="text/css" rel="stylesheet">

</head>
<body>
    <div class="container-fluid">
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>Contatos</h4>
                        </div>
                        <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar"  placeholder="Buscar" >
                                <span class="input-group-addon">
                                    <button type="button"> 
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span> 
                            </div>
                        </div>
                    </div>
                    <div class="inbox_chat">
                        <div class="chat_list active_chat">
                            <div class="chat_people">
                                <div class="chat_img"> 
                                    <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> 
                                </div>
                                <div class="chat_ib">
                                    <h5>Fulano
                                        <span class="chat_date">09 de Jun</span>
                                    </h5>
                                    <p>Oi, deixei uma mensagem, se puder me retorna...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mesgs">
                    <div class="msg_history">
                        <div class="incoming_msg">
                            <div class="incoming_msg_img"> 
                                <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> 
                            </div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>Boa tarde, estou com problema no meu sistema, poderia verificar?</p>
                                    <span class="time_date"> 11:01 - 09 de Junho</span>
                                </div>
                            </div>
                        </div>
                        <div class="outgoing_msg">
                            <div class="sent_msg">
                                <p>Ok, irei verificar e mais tarde retorno, pode ser?</p>
                                <span class="time_date"> 11:01 - 09 de Junho</span> 
                            </div>
                        </div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" placeholder="Digite sua mensagem" />
                            <button class="msg_send_btn" type="button">
                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".msg_history").animate({ 
                scrollTop: $(document).height() }, 
            "slow");

            return false;
        });
    </script>
</body>
</html>