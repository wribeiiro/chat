<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model('Chat_Model', 'chat');
		$this->load->library('Controle_Acesso', 'controle_acesso');
    }
    
	public function index() {
		$this->dados['titulo'] = 'Chat';
		$this->load->view('Chat/index', $this->dados);
	}
}
/* End of file Chat.php */
/* Location: ./application/controllers/Chat.php */