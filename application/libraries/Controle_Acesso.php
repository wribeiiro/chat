<?php 
    class Controle_Acesso {

        public function __construct() {

        }
        /**
         * Funcao para controlar a sessao 
         * e armazenar os dados do usuario.
         * Usaremos sempre no construtor da classe no controller
         */
        public function controlar() {
            $CI   = &get_instance();
            $user = $CI->session->userdata("sessao");
            
            if (empty($user)):
                $CI->session->set_flashdata('erro_login', 'Você não tem permissão para acessar este conteúdo!');
                redirect('');
            else:
                return $sessao = $user;
            endif;
        }

        /**
         * Verifica modulo do usuario, se tem permissao
         * @param mixed $id_usuario, $id_usuario
         */
        
        public function verificaModulo($id_usuario, $id_modulo) {
            $CI           = &get_instance();
            $model_user   = $CI->load->model('Usuarios_Model', 'usuarios');

            $this->dados['permissao'] = $CI->usuarios->getModulo($id_usuario, $id_modulo);

            if($this->dados['permissao'][0]->habilitado == 'S'):
                return true;
            else:
                $CI->session->set_flashdata('erro_modulo', 'Você não tem acesso ao(s) módulo(s)!');
            endif;
        }

        /**
         * Retorna Nível do usuario
         * @param mixed $nivel
         */

        public function retornaNivel($nivel) {
            switch($nivel) {
                case 'A':
                    $nivel_usuario = 'Administrador';
                break;
                case 'S':
                    $nivel_usuario = 'Supervisor';
                break;
                case 'V':
                    $nivel_usuario = 'Vendedor';
                break;
                case 'C':
                    $nivel_usuario = 'Comum';
                break;
            }

            return $nivel_usuario;
        }
        
    }
?>